# Toggle Layers - Some additional layer toggling - an Inkscape 1.2+ extension
# Will solo a selected sublayer within its own layer
# or cycle direct sub layers for the selected parent layer
# or show all siblings again for a selected layer
#
# Appears under Extensions>Arrange>Toggle Layers
#
# For Shortcut key use:Edit>Preferences>Keyboard>Effect>:
# app.inklinea.toggle-layers-cycle.noprefs
# app.inklinea.toggle-layers-show-all-siblings.noprefs
# app.inklinea.toggle-layers-solo.noprefs
