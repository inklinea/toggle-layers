#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Toggle Layers - Some additional layer toggling - an Inkscape 1.2+ extension
# Will solo a selected sublayer within its own layer
# or cycle direct sub layers for the selected parent layer
# or show all siblings again for a selected layer
#
# Appears under Extensions>Arrange>Toggle Layers
#
# For Shortcut key use:Edit>Preferences>Keyboard>Effect>:
# app.inklinea.toggle-layers-cycle.noprefs
# app.inklinea.toggle-layers-show-all-siblings.noprefs
# app.inklinea.toggle-layers-solo.noprefs
##############################################################################


import inkex

def cycle_visible_child(self, parent_layer):

    # Line below gives boolean
    # group_children = parent_layer.xpath('./child::svg:g/@inkscape:groupmode="layer"')

    # Find all immediate children which are layers
    child_layers = parent_layer.xpath('./child::svg:g[@inkscape:groupmode="layer"]')
    child_layers_count = len(child_layers)
    if child_layers_count < 1:
        return
    hidden_child_layers = parent_layer.xpath('./child::svg:g[@inkscape:groupmode="layer" and @style="display:none"]')
    hidden_child_layers_count = len(hidden_child_layers)
    #
    #
    # inkex.errormsg(hidden_child_layers)
    # inkex.errormsg(hidden_child_layers_count)
    # inkex.errormsg(child_layers_count)

    # Test if more than 1 layer is visible
    # What should we do if more than 1 child layer is visible ?
    # Just solo the first child layer

    for child_layer in child_layers:
        child_layer.style['display'] = 'none'

    if hidden_child_layers_count > child_layers_count-1 or hidden_child_layers_count < child_layers_count-1:
        inkex.errormsg('more than one sub layer visible, setting first layer visible')
        # for child_layer in child_layers:
        #     child_layer.style['display'] = 'none'
        try:
            # Remember paint order is -1
            del child_layers[-1].style['display']
        except KeyError:
            pass

    # Cycle if just one child visible
    # Make a long repeating index list so we can go back to beginning

    else:
        index_list = list(range(0, len(child_layers)))
        inkex_loop_list = index_list * 30
        # inkex.errormsg(inkex_loop_list)
        current_visible_layer = [x for x in child_layers if x not in hidden_child_layers][0]

        # inkex.errormsg(current_visible_layer.get('inkscape:label'))

        current_visible_layer_index = child_layers.index(current_visible_layer)

        # inkex.errormsg(current_visible_layer_index)

        # loop backwards through the loop list due to paint order
        loop_next_layer_index = inkex_loop_list[current_visible_layer_index+child_layers_count-1]

        try:
            # Remember paint order is -1
            del child_layers[loop_next_layer_index].style['display']
        except KeyError:
            pass


def solo_current_child(self, current_layer):

    parent_layer = current_layer.getparent()
    child_layers = parent_layer.xpath('./child::svg:g[@inkscape:groupmode="layer"]')
    child_layers_count = len(child_layers)

    if child_layers_count < 1:
        return

    for child_layer in child_layers:
        for child_layer in child_layers:
            child_layer.style['display'] = 'none'

    try:
        del current_layer.style['display']
    except KeyError:
        pass

def make_siblings_visible(self, current_layer):

    parent_layer = current_layer.getparent()
    child_layers = parent_layer.xpath('./child::svg:g[@inkscape:groupmode="layer"]')

    for child_layer in child_layers:
        try:
            del child_layer.style['display']
        except KeyError:
            pass



class ToggleLayers(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--toggle_type_string", type=str, dest="toggle_type_string", default='solo')

    def effect(self):

        current_layer = self.svg.get_current_layer()

        if self.options.toggle_type_string == 'show_all_siblings':
            make_siblings_visible(self, current_layer)

        if self.options.toggle_type_string == 'solo':
            solo_current_child(self, current_layer)

        if self.options.toggle_type_string == 'cycle':
            cycle_visible_child(self, current_layer)


if __name__ == '__main__':
    ToggleLayers().run()